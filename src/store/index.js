import Vue from "vue";
import Vuex from "vuex";
import crypto from "crypto";
import firebase from "firebase/app";
import * as Server from "../server";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currImage: null,
    profile: null,
    $vConnect: null
  },
  mutations: {
    updateCurrImage(state, payload) {
      state.currImage = payload;
    },
    updateProfile(state, payload) {
      state.profile = payload;
    },
    setVConnect(state, payload) {
      state.$vConnect = payload;
    }
  },
  actions: {
    async login(ctx, payload) {
      const { user, method } = payload;
      const { type, id } = method;
      const requestTime = Date.now();
      const deviceType = 3;
      const regionCode = firebase.auth().languageCode || "cn";
      let loginId = "";
      const { photoURL: avatar, email, uid, displayName: nickName } = user;
      if (type === "email" || type === "phone") {
        loginId = id;
      } else {
        loginId = uid;
      }
      // 支持各种类型的接入=0，代表匿名登陆，=1，代表邮箱，=2代表手机，=3代表google，=4代表 facebook
      const thirdType =
        type === "email"
          ? 1
          : type === "phone"
          ? 2
          : type === "google"
          ? 3
          : type === "facebook"
          ? 4
          : 0;
      const deviceCode = uid;
      const md5 = crypto.createHash("md5");
      const substr = "sfejwrew%#@!IVFgfre4j.497670dsdewSLDA";
      const sign = md5.update(loginId + requestTime + substr).digest("hex");
      const { status, data } = await Server.Login({
        loginId,
        thirdType,
        deviceCode,
        deviceType,
        requestTime,
        sign,
        regionCode,
        nickName,
        avatar,
        email
      });
      if (status === 0) {
        const { token, user: vUser } = data;
        localStorage.setItem("token", token);
        localStorage.setItem("profile", JSON.stringify(vUser));
        ctx.commit("updateProfile", vUser);
      }
    },
    async logout(ctx) {
      localStorage.removeItem("token");
      localStorage.removeItem("profile");
      await firebase.auth().signOut();
      ctx.commit("updateProfile", null);
    }
  },
  modules: {}
});

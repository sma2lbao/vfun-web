import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import ChatHome from "../views/ChatHome.vue";
// import Discover from "../views/Discover";
import Message from "../views/Message.vue";
import Personal from "../views/Personal.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    redirect: "/home",
    children: [
      {
        path: "home",
        name: "chat-home",
        component: ChatHome,
        // component: import(
        //   /* webpackChunkName: "chat-home" */ "../views/ChatHome.vue"
        // ),
        meta: {
          title: "Home",
          keep: false
        }
      },
      // {
      //   path: "discover",
      //   name: "discover",
      //   component: Discover,
      //   meta: {
      //     title: "Discover",
      //     keep: true
      //   }
      // },
      {
        path: "message",
        name: "message",
        component: Message,
        // component: import(
        //   /* webpackChunkName: "message" */ "../views/Message.vue"
        // ),
        meta: {
          title: "Message",
          keep: false
        }
      },
      {
        path: "personal",
        name: "personal",
        component: Personal,
        // component: import(
        //   /* webpackChunkName: "personal" */ "../views/Personal.vue"
        // ),
        meta: {
          title: "Personal",
          keep: true
        }
      }
    ],
    meta: {
      keep: false
    }
  },
  {
    path: "/chat-detail/:uid",
    name: "chat-detail",
    component: () =>
      import(/* webpackChunkName: "chat-detail" */ "../views/ChatDetail.vue"),
    meta: {
      keep: false
    }
  },
  {
    path: "/chatroom/:roomid",
    name: "chatroom",
    component: () =>
      import(/* webpackChunkName: "chatroom" */ "../views/ChatRoom.vue"),
    meta: {
      keep: false
    }
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
    meta: {
      title: "Login",
      keep: false
    }
  },
  {
    path: "/phone-login",
    name: "phone-login",
    component: () =>
      import(/* webpackChunkName: "phone-login" */ "../views/PhoneLogin.vue"),
    meta: {
      title: "Phone Login",
      keep: false
    }
  },
  {
    path: "/personal-edit",
    name: "personal-edit",
    component: () =>
      import(
        /* webpackChunkName: "personal-edit" */ "../views/PersonalEdit.vue"
      ),
    meta: {
      keep: false
    }
  },
  {
    path: "/recharge",
    name: "recharge",
    component: () =>
      import(/* webpackChunkName: "recharge" */ "../views/Recharge.vue"),
    meta: {
      keep: false
    }
  },
  {
    path: "/message-detail/:touid",
    name: "message-detail",
    component: () =>
      import(
        /* webpackChunkName: "message-detail" */ "../views/MessageDetail.vue"
      ),
    meta: {
      keep: false
    }
  }
];

const router = new VueRouter({
  mode: "hash",
  // base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition && to.meta.keep) {
      if (savedPosition === null) {
        to.meta.saved_position = null;
      } else {
        savedPosition = to.meta.saved_position;
      }
      let back = savedPosition ? savedPosition : { x: 0, y: 0 };
      return back;
    } else {
      return { x: 0, y: 0 };
    }
  }
});

router.beforeEach((to, from, next) => {
  // console.log(to);
  if (from.meta.keep) {
    from.meta.saved_position = { x: window.pageXOffset, y: window.pageXOffset };
  }
  next();
});
export default router;

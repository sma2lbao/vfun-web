import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "amfe-flexible";

import "vant/lib/icon/local.css";
import { Icon, Image, Lazyload, Button, Dialog, Toast, Locale } from "vant";

import enUS from "vant/lib/locale/lang/en-US";
import firebase from "firebase/app";
import "firebase/analytics";
import "firebase/auth";
import { firebaseConfig } from "./common/config";

Vue.use(Icon)
  .use(Image)
  .use(Lazyload)
  .use(Button)
  .use(Dialog)
  .use(Toast);

Locale.use("en-US", enUS);

Vue.config.productionTip = false;

firebase.initializeApp(firebaseConfig);
firebase.auth().useDeviceLanguage();
firebase.analytics();
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

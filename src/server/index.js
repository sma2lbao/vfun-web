import axios from "axios";
import router from "../router";
import Vue from "vue";
import { Toast } from "vant";

Vue.use(Toast);

axios.defaults.baseURL = "https://vfun.mixit.fun";
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";
axios.defaults.params = {
  toast: true
};

axios.interceptors.request.use(
  function(config) {
    if (!config.params || config.params.toast) {
      Toast.loading({
        message: "loading",
        duration: 0
      });
    }
    if (config.params && config.params.toast) {
      delete config.params.toast;
    }
    const token = localStorage.getItem("token");
    if (token) {
      config.headers["authorization"] = token;
    }
    // config.headers["authorization"] = "7a06bb464ece4a20a672d89f644fc67f";
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  function(response) {
    Toast.clear();
    if (response.data && response.data.status === 413) {
      router.replace("/login");
    }
    return response.data;
  },
  function(error) {
    Toast.fail("Busy server");
    return Promise.reject(error);
  }
);

// {
//   "avatar": "string",
//   "deviceCode": "string",
//   "deviceType": 0,
//   "email": "string",
//   "firebaseCode": "string",
//   "gender": "string",
//   "loginId": "string",
//   "nickName": "string",
//   "password": "string",
//   "regionCode": "string",
//   "requestTime": 0,
//   "sign": "string",
//   "thirdId": "string",
//   "thirdType": 0
// }
export const Login = params => {
  return axios.post("/vFun/user/login", params);
};

export const HomeList = (page = 1, type = 2, size = 20) => {
  return axios.post(
    "/vFun/user/list",
    {
      pageSize: size,
      query: {
        type: type
      },
      pageNum: page
    },
    {
      params: {
        toast: false
      }
    }
  );
};

export const UserInfo = code => {
  return axios.post("/vFun/user/info", {
    code
  });
};

export const DiscoverList = (page = 1, type = 2, size = 20) => {
  return axios.post(
    "/vFun/page/img",
    {
      pageSize: size,
      query: {
        type: type
      },
      pageNum: page
    },
    {
      params: {
        toast: false
      }
    }
  );
};

export const CommentList = objectId => {
  return axios.post("/comment/list", {
    objectId
  });
};

export const CommentSend = (objectId, comment, type) => {
  return axios.post("/comment/postcomment", {
    objectId,
    comment,
    type
  });
};

export const NewMessage = () => {
  return axios.post(
    "/vFun/message/unReadNum",
    {},
    {
      params: { toast: false }
    }
  );
};

export const Profile = () => {
  return axios.post(
    "/vFun/user/profile",
    {},
    {
      params: {
        toast: false
      }
    }
  );
};

export const EditProfile = params => {
  const bodyFormData = new FormData();
  Object.keys(params).map(key => {
    bodyFormData.append(key, params[key]);
  });
  return axios.post("/vFun/user/setProfile", bodyFormData, {
    headers: {
      "Content-Type": "multipart/form-data"
      // authorization: "7a06bb464ece4a20a672d89f644fc67f"
    }
  });
};

export const ProductList = () => {
  return axios.post("/vFun/product/list");
};

export const MessageList = (page = 1, size = 20) => {
  return axios.post(
    "/message/user/list",
    {
      pageSize: size,
      pageNum: page,
      query: {}
    },
    {
      params: {
        toast: false
      }
    }
  );
};

export const MessageSend = (content, relateUid, preMessageId = 0) => {
  return axios.post("/vFun/message/send", {
    content,
    preMessageId,
    relateUid
  });
};

export const MessageDetail = (page, relateUid, size = 20) => {
  return axios.post(
    "/message/user/detail",
    {
      pageSize: size,
      query: { relateUid },
      pageNum: page
    },
    {
      params: {
        toast: false
      }
    }
  );
};

// code	string
// allowEmptyValue: false
// 凭证或者token

// diamond	integer($int32)
// allowEmptyValue: false
// 购买的钻石

// money	string
// allowEmptyValue: false
// 消耗的money

// productId	string
// allowEmptyValue: false
// 购买的商品id

// type	integer($int32)
// allowEmptyValue: false
// 运行环境1正式2测试
export const PayByStripe = params => {
  const type = process.env.NODE_ENV === "production" ? 1 : 2;
  const json = Object.assign({}, params, { type });
  return axios.post("/vFun/stripe/create", json);
};

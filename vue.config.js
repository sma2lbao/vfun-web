module.exports = {
  publicPath: process.env.NODE_ENV === "test" ? "/vfun-web/" : "/",
  devServer: {
    https: false
  },
  // 生产环境 sourceMap
  productionSourceMap: false,
  configureWebpack: {
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000
      }
    },
    externals: {
      vue: "Vue",
      "vue-router": "VueRouter",
      vuex: "Vuex",
      axios: "axios",
      // jquery: "jQuery",
      vant: "vant",
      firebase: "firebase",
      "firebase/app": "firebase",
      "firebase/analytics": "firebase",
      "firebase/auth": "firebase",
      "intl-tel-input": "intlTelInput",
      "agora-rtc-sdk": "AgoraRTC"
    }
  },
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          "text-color": "#fff",
          "background-color": "#262626",
          "nav-bar-background-color": "#262626",
          "tabbar-background-color": "#262626",
          "tab-text-color": "#fff",
          "tabs-nav-background-color": "#262626",
          "tabs-bottom-bar-height": "0",
          "tabbar-item-icon-size": "28px",
          "tabbar-item-active-color": "#fff",
          "cell-background-color": "#262626",
          "cell-group-background-color": "#262626",
          "cell-group-title-color": "#fff",
          "cell-value-color": "#B5B5B5",
          "cell-right-icon-color": "#FEFEFE",
          "cell-active-color": "#333",
          "button-default-background-color": "#464646",
          "image-placeholder-background-color": "#D8D8D8",
          "dialog-width": "231px",
          "dialog-border-radius": "5px",
          "toast-background-color": "rgba(0,0,0, 0.8)"
        }
      }
    }
  }
};
